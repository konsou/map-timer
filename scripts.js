const TIMER_DEFAULT_MINUTES = 20;
const TIMER_DEFAULT_SECONDS = 0;
// DEFAULT VALUE FOR CLIENT: 20 min, 0 sec
let activeMapSegments;
let mapSegmentIndex = -1; // Currently darkened area - -1 = no areas dark yet
let userIsHost = false;

function uuidv4() {
    return ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g, c =>
      (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
    );
}

function startTimer(){
    console.log("Starting timer")
    $("#ftime").data("inProgress", "1");

    $("#pause").removeClass("active-button");
    $("#ftime").data("paused", "0");


    let minutes = $("#fminutes").val();
    let seconds = $("#fseconds").val();

    $("#ftime").data("minutes", minutes);
    $("#ftime").data("seconds", seconds);

    setTimerResetToValues(minutes, seconds);

    sendPageFullState();
}

function toggleTimerPause(){
    if ($("#ftime").data("paused") == "1"){
        resumeTimer();
    } else {
        pauseTimer();
    }
}

function pauseTimer(){
    console.log("Pausing timer");
    //$("#play-pause").addClass("active-button");
    $("#fminutes").prop("disabled", true);
    $("#fseconds").prop("disabled", true);
    $("#ftime").data("paused", 1);
    $("#ftime").data("inProgress", "0");
}

function resumeTimer(){
    // After pause - doesn't change resetToValues
    console.log("Resuming timer");
    //$("#play-pause").removeClass("active-button");
    $("#fminutes").prop("disabled", false);
    $("#fseconds").prop("disabled", false);
    $("#ftime").data("paused", "0");
    $("#ftime").data("inProgress", "1");

    let minutes = $("#fminutes").val();
    let seconds = $("#fseconds").val();

    $("#ftime").data("minutes", minutes);
    $("#ftime").data("seconds", seconds);
}

function stopAndResetTimer(){
    console.log("Stopping and resetting timer")
    $("#ftime").data("inProgress", "0");
    $("#fminutes").prop("disabled", false);
    $("#fseconds").prop("disabled", false);
    //$("#pause").removeClass("active-button");
    $("#ftime").data("paused", "0");

    //setTimerResetToValues(TIMER_DEFAULT_MINUTES, TIMER_DEFAULT_SECONDS);
    resetTimer();
}

function playPauseButtonPressed(){
    // do stuff
    let inProgress = parseInt($("#ftime").data("inProgress"));
    let paused = parseInt($("#ftime").data("paused"));
    console.log(`playPauseButtonPressed - inProgress: ${inProgress}, paused: ${paused}`);
    
    if ((!inProgress) && (!paused)){
        startTimer();
    } else {
        toggleTimerPause();
    }
}

function setTimerValues(minutes, seconds){
    minutes = parseInt(minutes);
    seconds = parseInt(seconds);

    //console.log(`setTimerValues - minutes: ${minutes}, seconds: ${seconds}`);

    $("#ftime").data("minutes", minutes);
    $("#ftime").data("seconds", seconds);
    $("#fminutes").val(minutes);
    $("#fseconds").val(seconds);
}

function setTimerResetToValues(minutes, seconds){
    console.log(`setting timer reset to values to ${minutes} minutes, ${seconds} seconds`);
    // Set the default values for timer

    //$("#ftime").data("reset-to-minutes", minutes);
    //$("#ftime").data("reset-to-seconds", seconds);
    let pageID = $("html").data("page-id");
    window.localStorage.setItem(`rpgHelperMapTimerPage${pageID}ResetToMinutes`, minutes);
    window.localStorage.setItem(`rpgHelperMapTimerPage${pageID}ResetToSeconds`, seconds);



    // TODO: send these to backend
}

function getTimerResetToValues(){
    let pageID = $("html").data("page-id");
    let resetToMinutes = window.localStorage.getItem(`rpgHelperMapTimerPage${pageID}ResetToMinutes`);
    let resetToSeconds = window.localStorage.getItem(`rpgHelperMapTimerPage${pageID}ResetToSeconds`);
    console.log(`in getTimerResetToValues: ${resetToMinutes} minutes, ${resetToSeconds} seconds`);

    return [ resetToMinutes, resetToSeconds ];
    //return [ $("#ftime").data("reset-to-minutes"), $("#ftime").data("reset-to-seconds") ]
}

function timerInProgress(){
    return parseInt($("#ftime").data("inProgress"));
}

function decrementTimer(){
    let minutes = $("#ftime").data("minutes");
    let seconds = $("#ftime").data("seconds");

    if (minutes <= 0 && seconds <= 0){
        throw "Timer finished";
    }

    seconds--;

    if (seconds < 0){
        minutes--;
        seconds += 60;
    }

    // Actual values stored in data attribute - NOT in UI!
    $("#ftime").data("minutes", minutes);
    $("#ftime").data("seconds", seconds);
    $("#fminutes").val(minutes);
    $("#fseconds").val(seconds);
}

function resetTimer(){
    let timerResetToValues = getTimerResetToValues();
    let resetToMinutes = timerResetToValues[0];
    let resetToSeconds = timerResetToValues[1];

    console.log(`Resetting timer to ${resetToMinutes} minutes, ${resetToSeconds} seconds`);

    // Actual values stored in data attribute - NOT in UI!
    $("#ftime").data("minutes", resetToMinutes);
    $("#ftime").data("seconds", resetToSeconds);
    $("#fminutes").val(resetToMinutes);
    $("#fseconds").val(resetToSeconds);
}

function shuffleArray(arr){
    for(let i = arr.length - 1; i > 0; i--){
        const j = Math.floor(Math.random() * i)
        const temp = arr[i]
        arr[i] = arr[j]
        arr[j] = temp
      }
}

function gatherAndShuffleMapSegments(){
    console.log("in gatherAndShuffleMapSegments");
    let mapSegments = [];

    $("svg g.togglable").each(function(currentElement){
        mapSegments.push(this);
    });
    console.log(mapSegments);
    shuffleArray(mapSegments);
    console.log(mapSegments);
    return mapSegments;
}

function gatherMapSegments(idList){
    //console.log("in gatherMapSegments");
    //console.log(idList);
    idList = JSON.parse(idList);
    let mapSegments = [];

    if (Array.isArray(idList)){
        idList.forEach(function(currentID){
            mapSegments.push($(`#${currentID}`));
        });
    }
    //console.log(mapSegments);
    return mapSegments;
}

function getIDsForElements(elementArray){
    let idArray = [];
    elementArray.forEach(function(currentElement){
        idArray.push(currentElement.id);
    });
    return idArray;
}

function darkenInactiveMapSegments(){
    $("svg g.inactive").children().addClass("inactive");
}

function getUserID(){
    // Return user UUID from local storage. Generate new user UUID if needed.
    let userID = window.localStorage.getItem("rpgHelperMapTimerUserID");

    if (userID == null){
        userID = uuidv4();
        window.localStorage.setItem("rpgHelperMapTimerUserID", userID);
    }

    //console.log(`user ID is ${userID}`);
    return userID;
}


function sendPageFullState(){
    //console.log("in sendPageFullState");
    let pageID = $("html").data("page-id");
    let minutes = $("#ftime").data("minutes");
    let seconds = $("#ftime").data("seconds");
    // timerInProgress();
    // getUserID();
    let activeGroupIDs = getIDsForElements(activeMapSegments);
    // mapSegmentIndex

    let updateData = {
        "page-id": pageID,
        "command": "update-page-full",
        "minutes": minutes,
        "seconds": seconds,
        "timer-in-progress": timerInProgress(),
        "user-id": getUserID(),
        "group-ids-list": activeGroupIDs,
        "group-active-index": mapSegmentIndex,
    }

    //console.log(updateData);

    let request = $.ajax({
        url: "ajax-calls.php",
        type: "post",
        data: updateData,
    });
    request.done(function (response, textStatus, jqXHR){
        //console.log(response);
        //response_parsed = JSON.parse(response);
    });
    request.fail(function (response, textStatus, jqXHR){
        console.log(`Error in sendPageFullState:`);
        console.log(textStatus);
        console.log(response);
        console.log(jqXHR);
        console.log(updateData);
    });  
}

function getPageFullState(){
    //console.log("in getPageFullState")
    
    let pageID = $("html").data("page-id");
  
    let request = $.ajax({
        url: "ajax-calls.php",
        type: "post",
        data: {
            "page-id": pageID,
            "command": "get-page-full",
        },
    });
    request.done(function (response, textStatus, jqXHR){
        //console.log(response);
        response_parsed = JSON.parse(response)[0];
        if (response_parsed){
            //console.log(response_parsed);

            if (getUserID() == response_parsed.host_identifier){
                console.log("current user is the host");
                userIsHost = true;
            }

            setTimerValues(response_parsed.minutes, response_parsed.seconds);
            if (parseInt(response_parsed.timer_in_progress) == 1){
                resumeTimer();
            }
            //response_parsed.timer_in_progress // do we need this?
            let newMapSegments = gatherMapSegments(response_parsed.group_ids_list); 
            if (newMapSegments.length > 0){ activeMapSegments = newMapSegments; }
            mapSegmentIndex = parseInt(response_parsed.group_active_index);

            darkenAreas(activeMapSegments, mapSegmentIndex);
        }


    });
    request.fail(function (response, textStatus, jqXHR){
        console.log(`Error in getPageFullState:`);
        console.log(textStatus);
        console.log(response);
        console.log(jqXHR);
    });  
}

function darkenAreas(activeMapSegments, mapSegmentIndex){
    //console.log(`darkenAreas - mapSegmentIndex: ${mapSegmentIndex}`);
    if (mapSegmentIndex > -1){
        for (let i = 0; i <= mapSegmentIndex; i++){
            //console.log(`darkening area ${$(activeMapSegments[i]).prop("id")}`);
            $(activeMapSegments[i]).children().addClass("darkened-area");
        }
    }
}

function claimPage(){
    let pageID = $("html").data("page-id");
    let request = $.ajax({
        url: "ajax-calls.php",
        type: "post",
        data: {
            "page-id": pageID,
            "command": "claim-page",
            "user-id": getUserID(),
        },
    });
    request.done(function (response, textStatus, jqXHR){
        console.log("claimPage response:")
        response_parsed = JSON.parse(response);
        console.log(response_parsed);
        if (response_parsed['claim-status'] == 1){
            console.log("page claimed")
            userIsHost = true;
        } else {
            console.log("couldn't claim page")
            userIsHost = false;
        }
    });
    request.fail(function (response, textStatus, jqXHR){
        console.log(`Error in getPageFullState:`);
        console.log(textStatus);
        console.log(response);
        console.log(jqXHR);
    });  
}


$(document).ready(function(){
    console.log("ready");
    //setTimerResetToValues(TIMER_DEFAULT_MINUTES, TIMER_DEFAULT_SECONDS);

    activeMapSegments = gatherAndShuffleMapSegments();
    $("html").data("page-id", $("#page-id").val());

    getUserID();

    darkenInactiveMapSegments();
    //resetTimer();
    stopAndResetTimer();
    claimPage(); // claims the page if it doesn't exist yet
    getPageFullState();

    $("#play-pause").click(playPauseButtonPressed);
    //$("#start").click(startTimer);
    //$("#pause").click(toggleTimerPause);
    $("#stop").click(stopAndResetTimer);

    window.setInterval(function(){
        if (!userIsHost){
            getPageFullState();
            $("#timer-buttons").hide();
        } else {
            $("#timer-buttons").show();
        }

        if (timerInProgress()){
            try {
                decrementTimer();
            } catch (e) {
                console.log("Timer finished!");

                mapSegmentIndex++;

                if (mapSegmentIndex >= activeMapSegments.length){
                    console.log("All regions painted");
                    stopAndResetTimer();
                } else {
                    darkenAreas(activeMapSegments, mapSegmentIndex);
                    resetTimer();
                }
                
            }
            if (userIsHost){
                sendPageFullState();
            }
        
        }
		
	}, 1000);
})