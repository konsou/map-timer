<?php
define("ADMIN_EMAIL", "tomi.javanainen@gmail.com");
define("DEBUG_MODE", true); // CHANGE THIS TO FALSE IN PRODUCTION!!

define("DB_ADDRESS", "127.0.0.1");
define("DB_NAME", "progress_clocks");
define("DB_PAGES_TABLE", "rpg_mapclock_pages");
define("DB_EVENTS_TABLE", "rpg_mapclock_events");

?>