<?php
include_once("settings.php");
include_once("settings-secret.php");
include_once("functions.php");

$page_id = $_POST['id'] ?? ($_POST['page-id'] ?? "");
$command = $_POST['command'] ?? "";
$minutes = $_POST['minutes'] ?? 0;
$seconds = $_POST['seconds'] ?? 0;
$timer_in_progress = $_POST['timer-in-progress'] ?? 0;
$user_id = $_POST['user-id'] ?? "";
$group_ids_list = $_POST['group-ids-list'] ?? "[]";
$group_ids_list = json_encode($group_ids_list);
$group_active_index = $_POST['group-active-index'] ?? 0;


function return_error(string $message, int $html_status_code = 400){
    http_response_code(400); // Bad Request
    echo json_encode(array("error" => $message));
    die();
}

// ERROR HANDLING
if ($command == ""){ return_error("command not set");}
if ($page_id == ""){ return_error("page-id not set");}


$conn = connect_db();

//$exists = page_exists($conn, $page_id) ? "yes" : "no";
//echo "Page id {$page_id} exists? ->{$exists}<-";

if ($command == "get-page-full"){
    $active_clocks = get_page_full_state($conn, $page_id);
    echo json_encode($active_clocks);
} else if ($command == "claim-page"){
    $success = intval(claim_page($conn, $page_id, $user_id));
    echo json_encode(array("claim-status" => $success));
} else if ($command == "update-page-full"){
    if (page_exists($conn, $page_id)){
        if ($user_id == get_page_host($conn, $page_id)){
            create_or_update_page($conn, $page_id, $minutes, $seconds, $timer_in_progress, $user_id, $group_ids_list, $group_active_index);
        } else {
            return_error("user {$user_id} is not the host for page {$page_id}, can't modify", 403);
        }
        
    } else {
        create_or_update_page($conn, $page_id, $minutes, $seconds, $timer_in_progress, $user_id, $group_ids_list, $group_active_index);
    }
}

$conn = null;    

?>