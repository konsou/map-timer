<?php
include_once("settings.php");
include_once("settings-secret.php");

function loadSVGElementFromFile($filename){
    // TODO: make safe (could upload a malicious svg file with HTML + JS code)
    // Svg file should contain groups to work with the system
    $svgFromFile = file_get_contents($filename);
    $svgExploded = explode("\n", $svgFromFile);
    if (substr($svgExploded[0], 0, 5) == "<?xml"){
        $svgExploded = array_slice($svgExploded, 1);
        $svgFromFile = join("\n", $svgExploded);
    }
    return $svgFromFile;
}

function print_database_error_message_and_die($error_data){
    http_response_code(500); // Internal Error

    $return_array = array("error" => "Database connection failed. Please notify site admin: ".ADMIN_EMAIL);
    if (DEBUG_MODE){
        $return_array['error-data'] = $error_data;
    }
    print_r($return_array);
    echo json_encode($return_array);
    die();
}

function connect_db(){
    // CONNECT TO DB
    try {
        $conn = new PDO("mysql:host=".DB_ADDRESS.";dbname=".DB_NAME, DB_USERNAME, DB_PASSWORD);
        // set the PDO error mode to exception
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
    catch(PDOException $e){
        print_database_error_message_and_die($e);
        }
    
        return $conn;
}

function page_exists($conn, string $page_id): bool {
    try {
        $stmt = $conn->prepare("SELECT COUNT(*) as count FROM ".DB_PAGES_TABLE." WHERE id = ?");
        $stmt->execute([$page_id]);
        $stmt->setFetchMode(PDO::FETCH_ASSOC);
        $result = $stmt->fetch();

        return ($result['count'] > 0);
    }
    catch(PDOException $e) {
        print_database_error_message_and_die($e);
        }

}

function get_page_full_state($conn, string $page_id): array{
    try {
        $stmt = $conn->prepare("SELECT * FROM ".DB_PAGES_TABLE." WHERE id = ?");
        $stmt->execute([$page_id]);
        $stmt->setFetchMode(PDO::FETCH_ASSOC);
        $result = $stmt->fetchAll();

        return $result;
    }
    catch(PDOException $e) {
        print_database_error_message_and_die($e);
        }
}

function get_page_host($conn, string $page_id): string{
    try {
        $stmt = $conn->prepare("SELECT host_identifier FROM ".DB_PAGES_TABLE." WHERE id = ?");
        $stmt->execute([$page_id]);
        $stmt->setFetchMode(PDO::FETCH_ASSOC);
        $result = $stmt->fetch();

        return $result['host_identifier'];
    }
    catch(PDOException $e) {
        print_database_error_message_and_die($e);
        }
}

function create_or_update_page($conn, string $page_id, int $minutes, int $seconds, int $timer_in_progress, string $host_identifier, string $group_ids_list, int $group_active_index){
    try {
        $sql = "REPLACE INTO ".DB_PAGES_TABLE." (id, minutes, seconds, timer_in_progress, host_identifier, group_ids_list, group_active_index) VALUES (?, ?, ?, ?, ?, ?, ?)";
        $stmt = $conn->prepare($sql);
        $stmt->execute([$page_id, $minutes, $seconds, $timer_in_progress, $host_identifier, $group_ids_list, $group_active_index]);
        }
    catch(PDOException $e){
        print_database_error_message_and_die($e);
        }
}

function claim_page($conn, string $page_id, string $user_id): bool{
    if (!page_exists($conn, $page_id)){
        create_or_update_page($conn, $page_id, 20, 0, 0, $user_id, "[]", -1);
        return true;
    } else {
        return (get_page_host($conn, $page_id) == $user_id);
    }
    
}

/*
function update_clock($conn, int $clock_id, int $segments, int $segment_value_int, string $text){
    $text = substr($text, 0, TITLE_MAX_LENGTH);
    
    // segment values as packed int
    try {
        $sql = "UPDATE clocks SET segments=?, segment_value_int=?, text=? WHERE id=?";
        $stmt = $conn->prepare($sql);
        $stmt->execute([$segments, $segment_value_int, $text, $clock_id]);
        }
    catch(PDOException $e){
        if (DEBUG_MODE) { echo $e; }
        else { print_database_error_message_and_die(); }
        }

}

function update_clock_title($conn, int $clock_id, string $text){
    $text = substr($text, 0, TITLE_MAX_LENGTH);
    
    try {
        $sql = "UPDATE clocks SET text=? WHERE id=?";
        $stmt = $conn->prepare($sql);
        $stmt->execute([$text, $clock_id]);
        }
    catch(PDOException $e){
        if (DEBUG_MODE) { echo $e; }
        else { print_database_error_message_and_die(); }
        }
}

function update_clock_segments($conn, int $clock_id, int $segment_value_int){
    try {
        $sql = "UPDATE clocks SET segment_value_int=? WHERE id=?";
        $stmt = $conn->prepare($sql);
        $stmt->execute([$segment_value_int, $clock_id]);
        }
    catch(PDOException $e){
        if (DEBUG_MODE) { echo $e; }
        else { print_database_error_message_and_die(); }
        }
}


function delete_clock($conn, int $clock_id){
    try {
        $sql = "DELETE FROM clocks WHERE id=?";
        $stmt = $conn->prepare($sql);
        $stmt->execute([$clock_id]);
        }
    catch(PDOException $e){
        if (DEBUG_MODE) { echo $e; }
        else { print_database_error_message_and_die(); }
        }

}

function handle_event($conn, string $session_id, int $clock_id = 0, string $event_type, string $event_data = ""){
    if (in_array($event_type, array("delete", "update_title", "update_segments")) && $clock_id == 0){
        echo json_encode(array("error" => "handle_event for {$event_type} needs clock_id (was {$clock_id})")); 
        return;
    }
    if ($event_type == "delete"){
        delete_clock($conn, $clock_id);
    }
    else if ($event_type == "update_title"){
        update_clock_title($conn, $clock_id, $event_data);
    }
    else if ($event_type == "update_segments"){
        update_clock_segments($conn, $clock_id, intval($event_data));
    }
}

function add_event($conn, string $session_id, int $clock_id = 0, string $event_type, string $event_data = ""){
    echo "in add_event - session_id: {$session_id}, event_data:";
    
    try {
        $sql = "INSERT INTO events (session_id, clock_id, type, data) VALUES (?, ?, ?, ?)";
        $stmt = $conn->prepare($sql);
        $stmt->execute([$session_id, $clock_id, $event_type, $event_data]);
        }
    catch(PDOException $e){
        if (DEBUG_MODE) { echo $e; }
        else { print_database_error_message_and_die(); }
        }
}

function get_events($conn, string $session_id, string $after_timestamp = ""){
    // timestamp in MySQL format: 2020-04-03 16:03:51.473

    try {
        $stmt = $conn->prepare("SELECT *, CURRENT_TIMESTAMP(3) AS 'query_timestamp' FROM events WHERE session_id = ? AND timestamp > ? ORDER BY timestamp ASC");
        $stmt->execute([$session_id, $after_timestamp]);
        $stmt->setFetchMode(PDO::FETCH_ASSOC);
        $result = $stmt->fetchAll();

        return $result;
    }
    catch(PDOException $e) {
        if (DEBUG_MODE) { echo $e; }
        else { print_database_error_message_and_die(); }
        }

}
*/
?>