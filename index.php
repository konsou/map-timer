<?php 
include_once("functions.php");
include_once("settings.php");

$this_page = parse_url($_SERVER["REQUEST_URI"], PHP_URL_PATH);

if (isset($_GET['id'])){ 
   $page_id = $_GET['id']; 
}
else { 
   // If no page id set: generate new page id, redirect to page with id as GET parameter
   if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on')   
      $url = "https://";   
   else  
      $url = "http://";   
   // Append the host(domain name, ip) to the URL.   
   $url.= $_SERVER['HTTP_HOST'];   
   
   // Append the requested resource location to the URL   
   $url.= $_SERVER['REQUEST_URI'];   
   $page_id = uniqid();

   header("Location: {$url}?id={$page_id}");
   exit();
}

//$svgFromFile = loadSVGElementFromFile("Continents.svg");
$svgFromFile = loadSVGElementFromFile("Sotakarjut_kartta-01-modified.svg");

?>
<!doctype html>
<html>
   <head>
      <title>Map Countdown</title>
      <script src="jquery-3.4.1.min.js"></script>
      <script src="scripts.js"></script>
      <meta charset="UTF-8">
      <link rel="stylesheet" href="styles.css" />
	   <!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-163209301-1"></script>
		<script>
         window.dataLayer = window.dataLayer || [];
         function gtag(){dataLayer.push(arguments);}
         gtag('js', new Date());

         gtag('config', 'UA-163209301-1');
		</script>
   </head>
   <body>
      <div id="header">
            <a href="<?php echo $this_page.'?id='.$page_id; ?>">Linkki tälle sivulle</a>
            - <a href="<?php echo $this_page;?>" target="_blank">Uusi tyhjä sivu</a>
            - <a href="https://rpghelpers.com">Rpghelpers.com</a>
      </div>

      <div id="timer">
         <form id="ftime">
            <input id="fminutes" name="fminutes" type="text" class="time-input" style="text-align:right;" size="2" />
            <label for="fminutes">minuuttia</label>
            <br>
            <input id="fseconds" name="fseconds" type="text" class="time-input" style="text-align:right;" size="2" />
            <label for="fseconds">sekuntia</label>
            <input type="hidden" name="page-id" id="page-id" value="<?php echo $page_id; ?>">
         </form>
         <div id="timer-buttons">
               <span id="timer-host-helper-text">Olet tämän sivun omistaja</span><br>
               <button type="button" id="play-pause" title="Käynnistä / tauko">⏯</button>
               <!--
               <button type="button" id="start">⏵</button>
               <button type="button" id="pause">⏸</button>
               -->
               <button type="button" id="stop" title="Pysäytä">⏹</button>
           </div>
      </div>

      <?php echo $svgFromFile; ?>

      <div id="footer">
         Copyright © 2020 Tomi Javanainen - <a href="https://bitbucket.org/konsou/map-timer/src/master/LICENSE.md" target="_blank">lisenssi (MIT)</a><br>
         Palautetta? Bugaako ohjelma? Toiveita uusista ropetuksen apuohjelmista? Lähetä mailia: <a href="mailto:tomi.javanainen@gmail.com">tomi.javanainen@gmail.com</a><br>
         Oliko tekeleestäni sinulle hyötyä? Osta koodaajalle kupillinen kahvia Ko-fi:ssa!<br>
			<a href="https://ko-fi.com/konso" target="_blank"><img src="images/Ko-fi_Logo_RGB.png" id="ko-fi-logo" alt="Ko-fi coffee cup logo" /></a>
      </div>
   </body>
</html>
